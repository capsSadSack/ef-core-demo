﻿using ConsoleApp;
using ConsoleApp.Database;
using ConsoleApp.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (BloggingContext context = BloggingRegistry.GetContext())
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var transaction = context.Database.BeginTransaction();

                #region adding blogs

                Blog blog1 = new Blog()
                {
                    Id = 1,
                    Url = @"https://www.artofmanliness.com/",
                    Name = "Art Of Manliness",
                    Rating = 5
                };
                context.Blogs.Add(blog1);

                Blog blog2 = new Blog()
                {
                    Id = 2,
                    Url = @"https://www.nomadicmatt.com/",
                    Name = "Nomadic Matt",
                    Rating = 4
                };
                context.Blogs.Add(blog2);

                Blog blog3 = new Blog()
                {
                    Id = 3,
                    Url = @"https://devblogs.microsoft.com/dotnet/",
                    Name = ".NET Blog",
                    Rating = 5
                };
                context.Blogs.Add(blog3);

                #endregion

                #region adding posts

                context.Posts.Add(new Post()
                {
                    Id = 1,
                    Title = @"Podcast #593: All You Have to Do Is Ask",
                    Content = "Some content...",
                    Blog = blog1
                });

                context.Posts.Add(new Post()
                {
                    Id = 2,
                    Title = @"Cool Uncle Tricks: Make a Salt Shaker Disappear",
                    Content = "Some content...",
                    Blog = blog1
                });

                context.Posts.Add(new Post()
                {
                    Id = 3,
                    Title = @"OUR NEW PATREON",
                    Content = "Some content...",
                    Blog = blog2
                });

                context.Posts.Add(new Post()
                {
                    Id = 4,
                    Title = @"THE 11 BEST THINGS TO SEE AND DO IN ISRAEL",
                    Content = "Some content...",
                    Blog = blog2
                });

                context.Posts.Add(new Post()
                {
                    Id = 5,
                    Title = @"Announcing .NET MAUI support for .NET 7 Release Candidate 2",
                    Content = "Some content...",
                    Blog = blog3
                });

                context.Posts.Add(new Post()
                {
                    Id = 6,
                    Title = @"What’s new in System.Text.Json in .NET 7",
                    Content = "Some content...",
                    Blog = blog3
                });

                context.Posts.Add(new Post()
                {
                    Id = 7,
                    Title = @".NET October 2022 Updates – .NET 6.0.10 and .NET Core 3.1.30",
                    Content = "Some content...",
                    Blog = blog3
                });

                #endregion

                TrySaveChanges(context);
                transaction.Commit();

                var urls = context.Blogs.OrderByDescending(b => b.Url)
                    .ThenByDescending(b => b.Posts.Count)
                    .Select(p => p.Url);

                foreach (var url in urls)
                {
                    Console.WriteLine(url);
                }

            }

            //var test = context.Blogs.FirstOrDefault();
            //Console.WriteLine($"{test.Id} {test.Url} {test.Name} {test.Rating}");
        }

        private static void TrySaveChanges(DbContext context)
        {
            try
            {
                bool hasChanges = context.ChangeTracker.HasChanges();
                int updates = context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

