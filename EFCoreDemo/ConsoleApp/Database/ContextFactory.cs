﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ConsoleApp.Database
{
    internal class ContextFactory : IDesignTimeDbContextFactory<BloggingContext>
    {
        public BloggingContext CreateDbContext(string[] args)
        {
            string connectionString = System.Configuration.ConfigurationManager.
                ConnectionStrings["DefaultConnection"].ConnectionString;

            DbContextOptionsBuilder<BloggingContext> optionsBuilder = 
                new DbContextOptionsBuilder<BloggingContext>()
                .UseSqlite(connectionString);

            return new BloggingContext(optionsBuilder.Options);
        }
    }
}
