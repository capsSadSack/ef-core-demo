﻿using ConsoleApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite.Infrastructure.Internal;

namespace ConsoleApp.Database
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; } = null!;
        public DbSet<Post> Posts { get; set; } = null!;


        public BloggingContext(DbContextOptions<BloggingContext> options)
            : base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var sqlServerOptionsExtension =
                optionsBuilder.Options.FindExtension<SqliteOptionsExtension>();
            if (sqlServerOptionsExtension != null)
            {
                string connectionString = sqlServerOptionsExtension.ConnectionString;
                optionsBuilder.UseSqlite(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Fluent API
            modelBuilder.Entity<Post>(post =>
            {
                post.HasKey(p => new { p.Id }).HasName("PK_Post");

                //post.Property(p => p.Title).IsRequired();
                //post.Property(p => p.Content).IsRequired();
            });

            modelBuilder.Entity<Blog>(blog =>
            {
                blog.HasKey(b => new { b.Id }).HasName("PK_Blog");

                //blog.Property(b => b.Name).IsRequired();
                //blog.Property(b => b.Url).IsRequired();

                //blog.HasMany(b => b.Posts)
                //    .WithOne(p => p.Blog)
                //    .HasForeignKey(p => p.Id);
            });
        }
    }
}
