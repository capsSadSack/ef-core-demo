﻿using ConsoleApp.Database;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    internal static class BloggingRegistry
    {
        public static BloggingContext GetContext()
        {
            string connectionString = System.Configuration.ConfigurationManager.
                ConnectionStrings["DefaultConnection"].ConnectionString;

            SqliteConnection connection = new SqliteConnection(connectionString);

            BloggingContext context = new BloggingContext(
                new DbContextOptionsBuilder<BloggingContext>()
                .UseSqlite(connection)
                .Options);

            return context;
        }
    }
}
